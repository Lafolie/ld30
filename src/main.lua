require "requireAll"

--gameloop callbacks
love.load = function()
	game = Game()
	
	love.keyboard.setTextInput(nil)
	love.handlers.reset = function() game:__init__() end
end

love.update = function(dt)
	local t = love.timer.getTime()
	game:update(dt, t)
end

love.draw = function()
	game:draw()
end

-- m&k input callbacks
love.keypressed = function(key, unicode)
	game:keypressed(key, unicode)
end

love.keyreleased = function(key)
	game:keyreleased(key)
end

love.mousepressed = function(x, y, button)
	game:mousepressed(x, y, button)
end

love.mousereleased = function(x, y, button)
	game:mousereleased(x, y, button)
end

love.mousefocus = function(f)
	game:mousefocus(f)
end

love.textinput = function(text)
	game:textinput(text)
end

--pad input callbacks (todo: update the löve wiki)
love.joystickadded = function(joystick)
	game:joystickadded(joystick)
end

love.joystickremoved = function(joystick)
	game:joystickremoved(joystick)
end

love.gamepadpressed = function(joystick, button)
	game:gamepadpressed(joystick, button)
end

love.gamepadreleased = function(joystick, button)
	game:gamepadreleased(joystick, button)
end

love.gamepadaxis = function(joystick, axis)
	game:gamepadaxis(joystick, axis)
end

--application callbacks
love.focus = function(f)
	game:focus(f)
end

love.visible = function(v)
	game:visible(v)
end

love.quit = function()
	game:quit()
end