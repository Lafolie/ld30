return {
	reset = function(self, mute)
	self.map = Map(1)
	self.player = PlayerController({x=220,y=1180},self.map)
	self.ai = AiManager(self.map,self.player)
	self.scroll = {x = 0,y = 0}
	self.hud = cache.image("gfx/overlay.png")
	end,			
		
	update = function(self, dt)
	self.player:update(dt)
	self.ai:update(dt)
	self.scroll = self.player:getScroll()
	
	if self.player.respawnTimer < 0 then
		game:changeState("init")
	end
	
	end,

	draw = function(self)
	love.graphics.scale(game.scale)
	if self.player.falling then
		self.player:draw(self.scroll)
	end
	self.map:draw(self.scroll)
	if not self.player.falling then
		self.player:draw(self.scroll)
	end
	self.ai:draw(self.scroll)
	
	love.graphics.draw(self.hud,0,10)
	love.graphics.print(self.player.hp,30,22)
	love.graphics.print(self.player.score,65,58)
	if self.player.dead then
		love.graphics.print("GAME OVER",300,15)
	end
	if self.player.falling then
		love.graphics.print("WATCH YOUR STEP",280,40)
	end
	end,
	
	mousepressed = function(self, x, y, button)
		self.player:mousepressed(x, y, button)
	end,

	keypressed = function(self, key, unicode)
	if key == "tab" then
		--self.map:switchLayer({x = self.player.playerPawn.position.x+16, y = self.player.playerPawn.position.y+32})
		self.map:switchLayer(self.player:getPosition())
		
	end
	end
}