class "TileSheet"
{
	__init__ = function(self, imagePath, tileSize)
		self.image = cache.image(imagePath)
		self.imageWidth = self.image:getWidth()
		self.imageHeight = self.image:getHeight()
		self.quads = {}
		
		for i=1,self.imageWidth/tileSize.x,1 do
			self.quads[i] = {}
			for j=1,self.imageHeight/tileSize.y,1 do
				self.quads[i][j] = love.graphics.newQuad(((i-1)*(tileSize.x+1)), ((j-1)*(tileSize.y+1)), tileSize.x, tileSize.y, self.imageWidth, self.imageHeight)
			end
		end
		
	end
}