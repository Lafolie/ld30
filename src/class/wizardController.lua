
class "WizardController"
{
	__init__ = function(self,spawnPosition,map,player)
		self.monsterPawn = Pawn(spawnPosition,TileSheet("gfx/wizard.png",{x=16,y=16}),100,50,-50,50)
		self.player = player
		self.map = map
		self.falling = false
		self.monsterPawn.animationLength =0.5
		self.monsterPawn.frameProgression = {1,2}
		self.attackRadius = 60
		self.agroRadius = 100
		self.colisionRadius = 8
		self.colisionOffset = {x=8,y=16}
		self.test = TileSheet("gfx/player.png",{x=1,y=1})
		self.attackTimer = love.math.random()
		self.attackSpeed = 1
		self.attackPower = 1
		self.layer = 4
		self.lastLandType = 4
		self.hp = 10
		
		self.monsterPawn.movementOverride = true
		
		self.bulletSpawnOffset = {x=16,y=18}
		self.bulletColisionOffset = {x = 4, y= 4}
		self.bullets = {}
		self.bulletRange = 1
		self.bulletDamage = 1
		self.bulletRadius = 1
		self.bulletTiles = TileSheet("gfx/iceMissile.png",{x=8,y=8})
	end,
		
	draw = function(self,scroll)
		love.graphics.draw(self.test.image,self.test.quads[1][1],self:getPosition().x, self:getPosition().y,0,1,1,scroll.x,scroll.y)
		self.monsterPawn:draw(scroll)
		for i, bullet in ipairs(self.bullets) do
			bullet:draw(scroll)
		end
	end,
	
	update = function(self, dt)
	
		for i, bullet in ipairs(self.bullets) do
			bullet:update(dt)
			if bullet.timeAlive > self.bulletRange then
				bullet.dead = true
			end
			end
		for i = #self.bullets , 1 ,-1 do
			if self.bullets[i].dead then
				table.remove(self.bullets,i)
			end
		end
	
		local t = self.map:getTileType(self:getPosition())  
		if t > 0 and bit.band(t,self.layer) ~= 0 then
			self.lastLandType = t
		end
		self.monsterPawn:update(dt)
		self:updatePawnFacing()
		--self:updatePawnAnimation()
		
		
		if not self.map:isFloor({x = self.monsterPawn.position.x+self.colisionOffset.x, y = self.monsterPawn.position.y+self.colisionOffset.y})then
			if not self.falling then
				print("AAGGGGGGGHHHHHHHH!!!!!")
				self.falling = true
			end
		else
			self.falling = false
		end
		
		self.attackTimer = self.attackTimer + dt
		
		if self.monsterPawn.colide(self.player:getPosition(),self.player.colisionRadius,self:getPosition(),self.attackRadius) then
			self:fireProjectile(self.player:getPosition())
			self:moveRandom(dt)
		elseif self.monsterPawn.colide(self.player:getPosition(),self.player.colisionRadius,self:getPosition(),self.agroRadius) then
			self:moveTowradsPlayer(dt)
		else
			self:moveRandom(dt)
		end
		
	end,
	
	attackPlayer = function(self)
		if self.attackTimer > self.attackSpeed then
			self.player:takeDamage(self.attackPower)
			self.attackTimer = 0
		end
	end,
	
	fireProjectile = function(self, targetPosition)
		if self.attackTimer > self.attackSpeed then
			local targetVector = {x = targetPosition.x - (self.monsterPawn.position.x + self.bulletSpawnOffset.x ) , y = targetPosition.y - (self.monsterPawn.position.y + self.bulletSpawnOffset.y)}
			local bullet = 	Pawn({x=self.monsterPawn.position.x + self.bulletSpawnOffset.x  - 4,y=self.monsterPawn.position.y + self.bulletSpawnOffset.y - 4},self.bulletTiles,200,200,0,0)
			bullet.desiredMovement = targetVector
			table.insert(self.bullets,bullet)
			self.attackTimer = 0
		end
	end,
	
	
	
	updatePawnAnimation = function(self)
		--print(self.monsterPawn.velocity.x)
		--[[
		if math.abs(self.monsterPawn.velocity.x) > 0 or math.abs(self.monsterPawn.velocity.y) > 0 then
			self.monsterPawn.animationLength =0.5
			self.monsterPawn.frameProgression = {4,5,6,7,8,9}
		else
			self.monsterPawn.animationLength =0.5
			self.monsterPawn.frameProgression = {1,2}
		end
		]]--
	end,
	
	updatePawnFacing = function(self)
		local playerPosition = self.player:getPosition()
		local pawnPosition = self:getPosition()
		local vectorToPlayer = {x = playerPosition.x - pawnPosition.x  , y = playerPosition.y - pawnPosition.y }
		if math.abs(vectorToPlayer.x) >  math.abs(vectorToPlayer.y)then
			if vectorToPlayer.x > 0 then
				self.monsterPawn.tileRow = 4
			else
				self.monsterPawn.tileRow = 3
			end
		else
			if vectorToPlayer.y >= 0 then
				self.monsterPawn.tileRow = 1
			else
				self.monsterPawn.tileRow = 2
			end
			
		end
	end,
	
	moveTowradsPlayer = function(self,dt)

		local playerPosition = self.player:getPosition()
		local pawnPosition = self:getPosition()
		--self.monsterPawn.desiredMovement = {x = playerPosition.x - pawnPosition.x   , y = playerPosition.y - pawnPosition.y }
		--self.monsterPawn.desiredMovement = {x = (playerPosition.x - pawnPosition.x) +((self.random-0.5)*(playerPosition.y - pawnPosition.y))  , y =( playerPosition.y - pawnPosition.y) +((self.random-0.5)*(playerPosition.x - pawnPosition.x))}
		self.monsterPawn.desiredMovement = {x = (playerPosition.x - pawnPosition.x)* self.monsterPawn.random +((self.monsterPawn.random-0.5)*(playerPosition.y - pawnPosition.y))  , y =( playerPosition.y - pawnPosition.y)* self.monsterPawn.random2  +((self.monsterPawn.random2-0.5)*(playerPosition.x - pawnPosition.x))}

		local desiredAccelerationMagnitude = math.sqrt(self.monsterPawn.desiredMovement.y^2 + self.monsterPawn.desiredMovement.x^2)
		local currentVelocityMagnitude = math.sqrt(self.monsterPawn.velocity.y^2 + self.monsterPawn.velocity.x^2)

		self.monsterPawn.acceleration.x = (desiredAccelerationMagnitude > 0 and (self.monsterPawn.desiredMovement.x/desiredAccelerationMagnitude) * self.monsterPawn.maxAcceleration or 0)
			+ ((currentVelocityMagnitude ~= 0 and not self.monsterPawn.velocity.x ~= 0) and ((self.monsterPawn.velocity.x/currentVelocityMagnitude) * self.monsterPawn.resistance) or 0)
		self.monsterPawn.acceleration.y = desiredAccelerationMagnitude > 0 and (self.monsterPawn.desiredMovement.y/desiredAccelerationMagnitude) * self.monsterPawn.maxAcceleration or 0
			+ ((currentVelocityMagnitude ~= 0 and not self.monsterPawn.velocity.y ~= 0) and ((self.monsterPawn.velocity.y/currentVelocityMagnitude) * self.monsterPawn.resistance) or 0)
		
		self.monsterPawn.velocity.x = self.monsterPawn.velocity.x + (self.monsterPawn.acceleration.x * dt * self.monsterPawn.maxAcceleration) 
		self.monsterPawn.velocity.y = self.monsterPawn.velocity.y + (self.monsterPawn.acceleration.y * dt * self.monsterPawn.maxAcceleration)
		
		desiredVelocityMagnitude = math.sqrt(self.monsterPawn.velocity.y^2 + self.monsterPawn.velocity.x^2)
		self.monsterPawn.velocity.x = (desiredVelocityMagnitude > 0 and desiredVelocityMagnitude > self.monsterPawn.velocityCutOff) and ((self.monsterPawn.velocity.x/desiredVelocityMagnitude) * math.min(desiredVelocityMagnitude,self.monsterPawn.maxVelocity)) or 0
		self.monsterPawn.velocity.y = (desiredVelocityMagnitude > 0 and desiredVelocityMagnitude > self.monsterPawn.velocityCutOff) and ((self.monsterPawn.velocity.y/desiredVelocityMagnitude) * math.min(desiredVelocityMagnitude,self.monsterPawn.maxVelocity)) or 0
		
		self.monsterPawn.position.x = self.monsterPawn.position.x + (self.monsterPawn.velocity.x * dt)
		self.monsterPawn.position.y = self.monsterPawn.position.y + (self.monsterPawn.velocity.y * dt)
		
		if not self.map:isFloor({x = self.monsterPawn.position.x+self.colisionOffset.x, y = self.monsterPawn.position.y+self.colisionOffset.y})then
			self.monsterPawn.position.x = pawnPosition.x - self.colisionOffset.x
			self.monsterPawn.position.y = pawnPosition.y - self.colisionOffset.y
			self.monsterPawn.velocity = {x = 0, y = 0}
			self.monsterPawn.acceleration = {x = 0, y = 0}
		end		
	end,
	
	moveRandom = function(self,dt)

		local playerPosition = self.player:getPosition()
		local pawnPosition = self:getPosition()
		self.monsterPawn.desiredMovement = {x = self.monsterPawn.random-0.5  , y =self.monsterPawn.random2 -0.5}

		local desiredAccelerationMagnitude = math.sqrt(self.monsterPawn.desiredMovement.y^2 + self.monsterPawn.desiredMovement.x^2)
		local currentVelocityMagnitude = math.sqrt(self.monsterPawn.velocity.y^2 + self.monsterPawn.velocity.x^2)

		self.monsterPawn.acceleration.x = (desiredAccelerationMagnitude > 0 and (self.monsterPawn.desiredMovement.x/desiredAccelerationMagnitude) * self.monsterPawn.maxAcceleration or 0)
			+ ((currentVelocityMagnitude ~= 0 and not self.monsterPawn.velocity.x ~= 0) and ((self.monsterPawn.velocity.x/currentVelocityMagnitude) * self.monsterPawn.resistance) or 0)
		self.monsterPawn.acceleration.y = desiredAccelerationMagnitude > 0 and (self.monsterPawn.desiredMovement.y/desiredAccelerationMagnitude) * self.monsterPawn.maxAcceleration or 0
			+ ((currentVelocityMagnitude ~= 0 and not self.monsterPawn.velocity.y ~= 0) and ((self.monsterPawn.velocity.y/currentVelocityMagnitude) * self.monsterPawn.resistance) or 0)
		
		self.monsterPawn.velocity.x = self.monsterPawn.velocity.x + (self.monsterPawn.acceleration.x * dt * self.monsterPawn.maxAcceleration) 
		self.monsterPawn.velocity.y = self.monsterPawn.velocity.y + (self.monsterPawn.acceleration.y * dt * self.monsterPawn.maxAcceleration)
		
		desiredVelocityMagnitude = math.sqrt(self.monsterPawn.velocity.y^2 + self.monsterPawn.velocity.x^2)
		self.monsterPawn.velocity.x = (desiredVelocityMagnitude > 0 and desiredVelocityMagnitude > self.monsterPawn.velocityCutOff) and ((self.monsterPawn.velocity.x/desiredVelocityMagnitude) * math.min(desiredVelocityMagnitude,self.monsterPawn.maxVelocity)) or 0
		self.monsterPawn.velocity.y = (desiredVelocityMagnitude > 0 and desiredVelocityMagnitude > self.monsterPawn.velocityCutOff) and ((self.monsterPawn.velocity.y/desiredVelocityMagnitude) * math.min(desiredVelocityMagnitude,self.monsterPawn.maxVelocity)) or 0
		
		self.monsterPawn.position.x = self.monsterPawn.position.x + (self.monsterPawn.velocity.x * dt)
		self.monsterPawn.position.y = self.monsterPawn.position.y + (self.monsterPawn.velocity.y * dt)
		
		if not self.map:isFloor({x = self.monsterPawn.position.x+self.colisionOffset.x, y = self.monsterPawn.position.y+self.colisionOffset.y})then
			self.monsterPawn.position.x = pawnPosition.x - self.colisionOffset.x
			self.monsterPawn.position.y = pawnPosition.y - self.colisionOffset.y
			self.monsterPawn.velocity = {x = 0, y = 0}
			self.monsterPawn.acceleration = {x = 0, y = 0}
		end		
	end,
	
	isSafeToMove = function(self,movement)
		return self.map:isFloor({x = self.monsterPawn.position.x+self.colisionOffset.x + movement.x, y = self.monsterPawn.position.y+self.colisionOffset.y + movement.y})
	end,
	
	stop = function(self)
		self.monsterPawn.desiredMovement = {x=0,y=0}
	end,
	
	takeDamage = function(self,damage)
		self.hp = self.hp - damage
		if self.hp <= 0 then
			self.dead = true
		end
	end,
	
	getPosition = function(self)
		return {x = self.monsterPawn.position.x + self.colisionOffset.x, y = self.monsterPawn.position.y+ self.colisionOffset.y}
	end
}