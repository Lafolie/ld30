
class "AiManager"
{
	__init__ = function(self,map,player)
		self.player = player
		self.map = map
		self.enimies ={}
		--self.slime = SlimeController({x=100,y=300},map,self.player)
		self:loadEnimies()
	end,
	
	loadEnimies = function(self,level)
		local image = cache.image("gfx/world.png")
		local imageData = image:getData()

		for x = 1, self.map.width, 1 do
			for y = 1, self.map.height, 1 do
				local r, g, b, a = imageData:getPixel( x-1, y-1 )
				if g == 255 or  r == 255  or  b == 255 then
					local tile = self.map.tiles[x][y]
					if(tile) and love.math.random() > 0.8 then
						self:spawnAi({x= x*16 - 16,y= y*16-24},tile:getTileType())
					end
				end
			end
		end
		
	end,
		
	draw = function(self,scroll)
	
		for k, v in pairs(self.enimies) do
			if bit.band(bit.band(v.layer,self.map.layers[self.map.currentLayer]),v.lastLandType) ~= 0 then 
				if(v:getPosition().y - scroll.y) > -100 and (v:getPosition().y - scroll.y) < 500  and (v:getPosition().x - scroll.x) > -100 and (v:getPosition().x - scroll.x) < 700 then
				v:draw(scroll)
				end
			end
		end
	end,
	
	update = function(self, dt)
	local scroll = self.player:getScroll()
		for k, v in pairs(self.enimies) do
			if bit.band(bit.band(v.layer,self.map.layers[self.map.currentLayer]),v.lastLandType) ~= 0 then
			if(v:getPosition().y - scroll.y) > -100 and (v:getPosition().y - scroll.y) < 500  and (v:getPosition().x - scroll.x) > -100 and (v:getPosition().x - scroll.x) < 700 then
				v:update(dt)
				if v.bullets then
					for i, bullet in ipairs(v.bullets) do
						if bullet.colide(self.player:getPosition(),self.player.colisionRadius,{x= bullet.position.x + v.bulletColisionOffset.x,y=bullet.position.y + v.bulletColisionOffset.y},v.bulletRadius) then
								self.player:takeDamage(v.bulletDamage)
								bullet.dead = true
						end
					end
				end
			end
			end
		end
		
		for i = #self.enimies , 1 ,-1 do
			if self.enimies[i].dead then
				table.remove(self.enimies,i)
			end
		end
		
		self:checkCollisionsPlayerBullets()	
	end,
	
	checkCollisionsPlayerBullets = function(self)
	local scroll = self.player:getScroll()
		for i, bullet in ipairs(self.player.bullets[self.map.currentLayer] ) do
			for k, enemy in pairs(self.enimies) do
				if(enemy:getPosition().y - scroll.y) > -100 and (enemy:getPosition().y - scroll.y) < 500  and (enemy:getPosition().x - scroll.x) > -100 and (enemy:getPosition().x - scroll.x) < 700 then
				if bit.band(bit.band(enemy.layer,self.map.layers[self.map.currentLayer]),enemy.lastLandType) ~= 0 then 
					if bullet.colide(enemy:getPosition(),enemy.colisionRadius,{x= bullet.position.x + self.player.bulletColisionOffset.x,y=bullet.position.y + self.player.bulletColisionOffset.y},self.player.bulletRadius) then
						enemy:takeDamage(self.player.bulletDamage)
						self.player.score = self.player.score +10
						bullet.timeAlive = bullet.timeAlive * 2
					end
				end
				end
			end
		end
	end,
	
	spawnAi = function(self,position,tileType)
		if tileType == 1 then
			local slime = SlimeController(position,self.map,self.player)
			table.insert(self.enimies,slime)
		elseif tileType == 2 then
			local flame = FlameController(position,self.map,self.player)
			table.insert(self.enimies,flame)
		elseif tileType == 3 then
			local wurm = WurmController(position,self.map,self.player)
			table.insert(self.enimies,wurm)
		elseif tileType == 4 then
			local wizard = WizardController(position,self.map,self.player)
			table.insert(self.enimies,wizard)
		end
		
		--local flame = FlameController({x=80,y=100},self.map,self.player)
		--table.insert(self.enimies,flame)
		
		--local wurm = WurmController({x=100,y=300},self.map,self.player)
		--table.insert(self.enimies,wurm)

	end
}