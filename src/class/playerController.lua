
class "PlayerController"
{
	__init__ = function(self,spawnPosition,map)
		self.playerPawn = Pawn(spawnPosition,TileSheet("gfx/cyborg.png",{x=32,y=32}),100,50,-10,10)
		self.map = map
		self.falling = false
		self.cursor = love.mouse.newCursor("gfx/crosshair.png", 7, 8)
		love.mouse.setCursor(self.cursor)
		self.colisionRadius = 8
		self.colisionOffset = {x=16,y=32}
		self.bulletSpawnOffset = {x=16,y=18}
		self.bullets = {}
		for k, v in pairs(self.map.layers) do
			self.bullets[k] = {}
		end
		self.bulletTiles = TileSheet("gfx/player.png",{x=16,y=16})
		self.bulletDamage = 5
		self.bulletRadius = 3
		self.bulletColisionOffset = {x = 8, y= 8}
		self.test = TileSheet("gfx/player.png",{x=1,y=1})
		self.hp = 1000
		
		self.bulletRange = 1
		
		self.score = 0
		
		self.respawnTimer = 5
	end,
		
	draw = function(self,scroll)
		self.playerPawn:draw(scroll)
		love.graphics.draw(self.test.image,self.test.quads[1][1],self:getPosition().x, self:getPosition().y,0,1,1,scroll.x,scroll.y)
		for i, bullet in ipairs(self.bullets[self.map.currentLayer] ) do
			bullet:draw(scroll)
		end
	end,
	
	update = function(self, dt)
		
		if self.dead then
			self.respawnTimer = self.respawnTimer - dt
		end
		
		self:movePawn()
		self.playerPawn:update(dt)
		self:updatePawnFacing()
		self:updatePawnAnimation()
		
		for k, v in pairs(self.map.layers) do
			for i, bullet in ipairs(self.bullets[k] ) do
				bullet:update(dt)
				if bullet.timeAlive > self.bulletRange then
					bullet.dead = true
				end
			end
			for i = #self.bullets[k] , 1 ,-1 do
			if self.bullets[k][i].dead then
				table.remove(self.bullets[k],i)
			end
		end
		end
		
		if not self.map:isFloor({x = self.playerPawn.position.x+ self.colisionOffset.x, y = self.playerPawn.position.y+self.colisionOffset.y})then
			if not self.falling then
				self.falling = true
				self.dead = true
				self.playerPawn.maxVelocity = 200
			end
		else
			--self.falling = false
		end
	end,
	
	updatePawnAnimation = function(self)
		if self.dead then
			self.playerPawn.animationLength =0.5
			self.playerPawn.frameProgression = {1}
			return
		end
	
		--print(self.playerPawn.velocity.x)
		if math.abs(self.playerPawn.velocity.x) > 0 or math.abs(self.playerPawn.velocity.y) > 0 then
			self.playerPawn.animationLength =0.5
			self.playerPawn.frameProgression = {4,5,6,7,8,9}
		else
			self.playerPawn.animationLength =0.5
			self.playerPawn.frameProgression = {1,2}
		end
	end,
	
	updatePawnFacing = function(self)
		if self.dead then
			return
		end
		local scroll = self:getScroll()
		local vectorToMouse = {x = love.mouse.getX()/game.scale +scroll.x - (self.playerPawn.position.x + self.bulletSpawnOffset.x ) , y = love.mouse.getY()/game.scale +scroll.y - (self.playerPawn.position.y + self.bulletSpawnOffset.y)}
		if math.abs(vectorToMouse.x) >  math.abs(vectorToMouse.y)then
			if vectorToMouse.x > 0 then
				self.playerPawn.tileRow = 4
			else
				self.playerPawn.tileRow = 3
			end
		else
			if vectorToMouse.y >= 0 then
				self.playerPawn.tileRow = 1
			else
				self.playerPawn.tileRow = 2
			end
			
		end
	end,
	
	mousepressed = function(self, x, y, button)
		if button == "l" and not self.dead then
			local scroll = self:getScroll()
			local mousePosition = {x = (x/game.scale)+scroll.x, y = (y/game.scale)+scroll.y}
			self:fireProjectile(mousePosition)
		end
	end,
	
	fireProjectile = function(self, targetPosition)
		local vectorToMouse = {x = targetPosition.x - (self.playerPawn.position.x + self.bulletSpawnOffset.x ) , y = targetPosition.y - (self.playerPawn.position.y + self.bulletSpawnOffset.y)}
		local bullet = 	Pawn({x=self.playerPawn.position.x + self.bulletSpawnOffset.x  - 4,y=self.playerPawn.position.y + self.bulletSpawnOffset.y - 4},self.bulletTiles,200,200,0,0)
		bullet.desiredMovement = vectorToMouse
		table.insert(self.bullets[self.map.currentLayer],bullet)
	end,
	
	getScroll = function(self)
		local scroll = {x = self.playerPawn.position.x - 300, y = self.playerPawn.position.y - 225}
		return scroll
	end,
	
	movePawn = function(self)
		if self.falling then
			self.playerPawn.desiredMovement.x = 0
			self.playerPawn.desiredMovement.y = 1
		else 
		
		if self.dead then
			return
		end
		
		if love.keyboard.isDown("right", "d") then
			self.playerPawn.desiredMovement.x = 1
		elseif love.keyboard.isDown("left","a") then
			self.playerPawn.desiredMovement.x = -1
		else
			self.playerPawn.desiredMovement.x = 0
		end
			
		if love.keyboard.isDown("up", "w") then
			self.playerPawn.desiredMovement.y = -1
		elseif love.keyboard.isDown("down","s") then
			self.playerPawn.desiredMovement.y = 1
		else
			self.playerPawn.desiredMovement.y = 0
		end
		
		end
	end,
		
	takeDamage = function(self,damage)
		self.hp = self.hp - damage
		if self.hp <= 0 then
			self.dead = true
		end
	end,
		
	
	
	getPosition = function(self)
		return {x = self.playerPawn.position.x + self.colisionOffset.x, y = self.playerPawn.position.y+ self.colisionOffset.y}
	end
	
	
}