class "Tile"
{
	__init__ = function(self,position,tileSheet,layers)
		self.position = position
		self.tileSheet = tileSheet
		self.tileType = {red = 1, green = 1, blue = 1 }
		self.layers = layers
		test = true
	end,
		
	draw = function(self,layer,scroll)
		local style = self:getTileStyle(layer)
		for k, v in pairs(style) do
			love.graphics.draw(self.tileSheet.image,self.tileSheet.quads[k][v],self.position.x*16 - 16, self.position.y*16 - 16,0,1,1,scroll.x,scroll.y)
		end
	end,
	
	getTileStyle = function(self,layer)
		local style = {}
		local currentType = self.tileType[layer]
		style[currentType] = 0
		for k, v in pairs(self.tileType) do
			for kk, vv in pairs(style) do	
				if ((bit.bxor(v-1,kk-1) ==  0)) or  v == 17 then
					if(bit.band(self.layers[k],style[kk]) == 0) then
						style[kk] = style[kk] + self.layers[k]
					end
				elseif (bit.band(v-1,kk-1) ~= 0) and v ~= 17 and kk ~= 17 then
					local crossover = bit.band(v-1,kk-1) +1
					local nonMatchingPart = ((kk-1)-(crossover-1))+ 1
					local s = style[kk]
					style[kk] = nil
					style[crossover] = s + self.layers[k]
					if nonMatchingPart > 1 then
						if(style[nonMatchingPart]) then
							style[nonMatchingPart] = s + style[nonMatchingPart]
						else
							style[nonMatchingPart] = s 
						end
					end
				end
			end
		end
		return style
		
	end,
	
	isFloor = function(self,layer)
		return self.tileType[layer] == 17
	end,
	
	getTileType = function(self)
		local tileType = 0
		for k, v in pairs(self.layers ) do
			if self:isFloor(k) then
				tileType = tileType + v
			end
		end
		return tileType
	end,
	
	update = function(self, dt)
	end,
}