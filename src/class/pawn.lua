class "Pawn"
{
	__init__ = function(self,spawnPosition,tileSheet,maxVelocity,maxAcceleration,resistance,velocityCutOff)
		self.position = spawnPosition
		
		self.maxVelocity = maxVelocity --pixls per s                  --TWEAKING*************************
		self.maxAcceleration = maxAcceleration--pixls per s^2			--TWEAKING*************************		
		self.resistance = resistance --pixls per s^2			--TWEAKING*************************
		self.velocityCutOff = velocityCutOff--pixls per s                  --TWEAKING*************************
		self.desiredMovement = {x=0,y=0}
		self.velocity = {x=0,y=0}
		self.acceleration = {x=0,y=0}
		
		self.random = love.math.random()
		self.random2 = love.math.random()
		
		
		--Animations
		self.tileSheet = tileSheet
		self.tileRow = 1
		self.tileColumn = 1
		self.animationTimer = self.random 
		self.animationLength =1
		self.frameProgression = {1}
		self.timeAlive = 0
		
	end,
		
	draw = function(self,scroll)
		love.graphics.draw(self.tileSheet.image,self.tileSheet.quads[self.tileColumn][self.tileRow],self.position.x, self.position.y,0,1,1,scroll.x,scroll.y)
	end,
	
	update = function(self, dt)
		if not self.movementOverride then
		
		local desiredAccelerationMagnitude = math.sqrt(self.desiredMovement.y^2 + self.desiredMovement.x^2)
		local currentVelocityMagnitude = math.sqrt(self.velocity.y^2 + self.velocity.x^2)

		self.acceleration.x = (desiredAccelerationMagnitude > 0 and (self.desiredMovement.x/desiredAccelerationMagnitude) * self.maxAcceleration or 0)
			+ ((currentVelocityMagnitude ~= 0 and not self.velocity.x ~= 0) and ((self.velocity.x/currentVelocityMagnitude) * self.resistance) or 0)
		self.acceleration.y = desiredAccelerationMagnitude > 0 and (self.desiredMovement.y/desiredAccelerationMagnitude) * self.maxAcceleration or 0
			+ ((currentVelocityMagnitude ~= 0 and not self.velocity.y ~= 0) and ((self.velocity.y/currentVelocityMagnitude) * self.resistance) or 0)
		
		self.velocity.x = self.velocity.x + (self.acceleration.x * dt * self.maxAcceleration) 
		self.velocity.y = self.velocity.y + (self.acceleration.y * dt * self.maxAcceleration)
		
		desiredVelocityMagnitude = math.sqrt(self.velocity.y^2 + self.velocity.x^2)
		self.velocity.x = (desiredVelocityMagnitude > 0 and desiredVelocityMagnitude > self.velocityCutOff) and ((self.velocity.x/desiredVelocityMagnitude) * math.min(desiredVelocityMagnitude,self.maxVelocity)) or 0
		self.velocity.y = (desiredVelocityMagnitude > 0 and desiredVelocityMagnitude > self.velocityCutOff) and ((self.velocity.y/desiredVelocityMagnitude) * math.min(desiredVelocityMagnitude,self.maxVelocity)) or 0
		
		self.position.x = self.position.x + (self.velocity.x * dt)
		self.position.y = self.position.y + (self.velocity.y * dt)
		
		end
		
		self:updateAnimation(dt)
		
		self.timeAlive = self.timeAlive+ dt
		
	end,
	
	updateAnimation = function(self,dt)
		self.animationTimer = self.animationTimer + dt
		if self.animationTimer > self.animationLength then
			self.animationTimer = self.animationTimer - self.animationLength
			self.random = love.math.random()
			self.random2 = love.math.random()
		end
		
		for k, v in ipairs(self.frameProgression) do
			if self.animationTimer < (self.animationLength/#self.frameProgression) * k and self.animationTimer > (self.animationLength/#self.frameProgression) * (k-1) then
					self.tileColumn = v
					
			end
		end
	end,
	
	colide = function(aPos,aR,bPos,bR)
		--print(aPos.x.."    "..bPos.x.."  "..aPos.y.."    "..bPos.y)
		return((aPos.x-bPos.x)^2 + (aPos.y-bPos.y)^2) < (aR+bR)^2 

	end
}