class "Map"
{
	__init__ = function(self,level)
	self.tileSheet = TileSheet("gfx/tiles.png",{x=16,y=16})
	self.layers = {green = 1,red = 2,blue = 4}
	self:generateMap(level)
	self.currentLayer = "green"
	end,
	
	generateMap = function(self,level)
		self.tiles = {}
		self.batches = {}
		local image = cache.image("gfx/world.png")
		local imageData = image:getData()
		self.width = imageData:getWidth()
		self.height = imageData:getHeight()

		for x = 1, self.width, 1 do
			table.insert(self.tiles,{})
			for y = 1, self.height, 1 do
				local r, g, b, a = imageData:getPixel( x-1, y-1 )
				local tile = Tile({x=x,y=y},self.tileSheet,self.layers)
				if g == 255 then
					tile.tileType.green = 17
				end
				if r == 255 then
					tile.tileType.red = 17
				end
				if b == 255 then
					tile.tileType.blue = 17
				end					
				table.insert(self.tiles[x],tile)
			end
		end
		
		for k, v in pairs(self.layers) do
			self:generateMapEdges(k)
		end
		
		for k, v in pairs(self.layers) do
			self:generateBatch(k)
		end
	end,
	
	generateMapEdges = function(self,layer)
		for x, v in ipairs(self.tiles) do
			for y, vv in ipairs(v) do
				if  vv:isFloor(layer) then
					if y < self.height and not self.tiles[x][y+1]:isFloor(layer) then
						self.tiles[x][y+1].tileType[layer] = self.tiles[x][y+1].tileType[layer] + 1
					end
					if x > 1 and not self.tiles[x-1][y]:isFloor(layer) then
						self.tiles[x-1][y].tileType[layer] = self.tiles[x-1][y].tileType[layer] + 2
					end
					if y > 1 and not self.tiles[x][y-1]:isFloor(layer) then
						self.tiles[x][y-1].tileType[layer] = self.tiles[x][y-1].tileType[layer] + 4
					end
					if x < self.width and not self.tiles[x+1][y]:isFloor(layer) then
						self.tiles[x+1][y].tileType[layer] = self.tiles[x+1][y].tileType[layer] + 8
					end
				end
			end
		end
	end,

	generateBatch = function(self,layer)
		local batch = love.graphics.newSpriteBatch(self.tileSheet.image, self.width*self.height)
		batch:bind()
		for x, v in ipairs(self.tiles) do
			for y, vv in ipairs(v) do
				for k, v in pairs(vv:getTileStyle(layer)) do
					--print(v.."  "..k)
					if v < 8 then
						batch:add(self.tileSheet.quads[k][v], vv.position.x*16 - 16, vv.position.y*16 - 16)
					end
				end
			end
		end
		batch:unbind()
		self.batches[layer] = batch
	end,
		
	draw = function(self,scroll)
		love.graphics.draw(self.batches[self.currentLayer], 0, 0,0,1,1,scroll.x,scroll.y)
	end,
	
	switchLayer = function(self,position)
		if self:isFloor(position) then
			local layer =  self.currentLayer
			for k, v in pairs(self.layers) do
					if self:isFloorForLayer(position,k) and k ~= self.currentLayer then
						if v == self.layers[self.currentLayer]*2 then
							self.currentLayer = k
							return
						elseif self.currentLayer == layer or v < self.layers[layer] then
							layer = k
						end
					end
			end
			self.currentLayer = layer
			return
		end
	end,
	
	isFloor = function(self,position)
		 return self:isFloorForLayer(position, self.currentLayer)
	end,
	
	isFloorForLayer = function(self,position,layer)
		local x = ((position.x - position.x%16)/16)+1
		local y = ((position.y - position.y%16)/16)+1
		return x > 0 and x <= self.width and y > 0 and  y <= self.height and self.tiles[x][y]:isFloor(layer)
	end,
	
	getTileType = function(self,position)
		local x = ((position.x - position.x%16)/16)+1
		local y = ((position.y - position.y%16)/16)+1
		if  x > 0 and x <= self.width and y > 0 and  y <= self.height then
			return self.tiles[x][y]:getTileType()
		else
			return 0
		end
	end,
	
	update = function(self, dt)
	end,
}